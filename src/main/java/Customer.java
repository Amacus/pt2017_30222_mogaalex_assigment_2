/**
 * Created by Amacus5 on 25.03.2017.
 */
public class Customer {
    private long arrivalTime;//random
    private int processingTime;//random
    private long waitingTime;
    private int no;


    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(long waitingTime) {
        this.waitingTime = waitingTime;
    }

}
