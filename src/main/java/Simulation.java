import java.util.Random;

/**
 * Created by Amacus5 on 02.04.2017.
 */
public class Simulation implements Runnable  {
    private int minArriving;
    private int maxArriving;
    private int minService;
    private int maxService;
    private int noQueues;
    private int simulationTime;
    Server server = new Server();
    Schedule schedule;


    public Simulation(int i, int i1, int i2, int i3, int i4, int i5) {
        minArriving=i;
        maxArriving=i1;
        minService=i2;
        maxService=i3;
        noQueues = i4;
        simulationTime=i5;
        schedule = new Schedule(noQueues);


    }

    private void generateCustomers(long arriving,int i){
        Random ran = new Random();
        int service = ran.nextInt(maxService)+minService;
        Customer customer = new Customer();
        customer.setArrivalTime(arriving);
        customer.setProcessingTime(service);
        customer.setNo(i);
        server = schedule.chooseServer();
        schedule.chooseServer().addCustomer(customer);
        customer.setWaitingTime(server.getWaitingPeriod().get()+customer.getArrivalTime()/1000);
        Main.view.getTextArea1().append("\nTime(s): "+arriving/1000+"\nCustomer "+ i + " has arrived ");
        Main.view.getTextArea1().append("\nCustomer "+ i + " goes to server" + server.getNo());

    }
    public void run() {
        long time = System.currentTimeMillis();
        int i = 1;
        Random ran = new Random();
        while (System.currentTimeMillis() < time + simulationTime * 1000) {
            try {
                int nextCustomer = ran.nextInt(maxArriving) + minArriving;
                Thread.sleep(nextCustomer * 1000);
                long arrivingT = System.currentTimeMillis() - time;
                generateCustomers(arrivingT, i);
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

            Main.view.getTextArea1().append("\nThe shop is closed");
        while(true) {
            int ok=0;
            for (Server s : schedule.getServers()) {
                if (s.getWaitingPeriod().get() != 0)
                    ok = 1;
            }
            if(ok == 0) {
                Main.view.getTextArea1().append("\n FINISH");
                Main.view.getTextArea1().append("\n The largest number of customers is " + Server.getMaxC() + " at time " +Server.getPeakHour()/1000);
                Main.view.getTextArea1().append("\n Average processing time is: "+Server.avgt());
                Main.view.getTextArea1().append("\n Average waiting time is: "+Server.avgWaiting());
                break;
            }
        }
    }
}
