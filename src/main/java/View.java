import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Amacus5 on 01.04.2017.
 */
public class View {
    public JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JButton startButton;
    private JTextArea textArea1;

    public JTextArea getTextArea1() {
        return textArea1;
    }



    public View() {
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea1.setText(null);
                textArea1.append("Start ...");
                Simulation simulation = new Simulation(Integer.parseInt(getTextField1()),Integer.parseInt(getTextField2()),Integer.parseInt(getTextField3()),Integer.parseInt(getTextField4()),Integer.parseInt(getTextField5()),Integer.parseInt(getTextField6()));
                Thread s1 = new Thread(simulation);
                s1.start();
            }
        });
    }

    public  String getTextField1() {
        return textField1.getText();
    }

    public String getTextField2() {
        return textField2.getText();
    }

    public String getTextField3() {
        return textField3.getText();
    }

    public String getTextField4() {
        return textField4.getText();
    }

    public String getTextField5() {
        return textField5.getText();
    }

    public String getTextField6() {
        return textField6.getText();
    }
}
