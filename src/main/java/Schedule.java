import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Amacus5 on 25.03.2017.
 */
public class Schedule {
    private List<Server> servers = Collections.synchronizedList(new ArrayList<Server>());
    private int maxServers;

    public Schedule(int maxServers) {
        this.maxServers = maxServers;
        Thread Threads[] = new Thread[maxServers];
        for(int i =0;i<maxServers;i++){
            Server s1 = new Server();
            s1.setNo(i);
            servers.add(s1);
            Threads[i] = new Thread(s1);
            Threads[i].start();
        }
    }
    public Server chooseServer(){
         return Collections.min(servers, new Comparator<Server>() {
            public int compare(Server o1, Server o2) {
                if (o1.getWaitingPeriod().get() >o2.getWaitingPeriod().get() )
                    return 1;
                else if (o1.getWaitingPeriod().get() < o2.getWaitingPeriod().get())
                    return -1;
                return 0;
            }
        });
    }

    public List<Server> getServers() {
        return servers;
    }

}
