import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Amacus5 on 25.03.2017.
 */
public class Server implements Runnable {
        private BlockingQueue<Customer> customers;
        private AtomicInteger waitingPeriod;
        private int no;
        private static  int noCustomers;
        private static int maxC;
        private static long peakHour;
        private static double avgProcessingTime;
        private static double avgWaitingTime;
        private static int aux;


    static long getPeakHour() {
        return peakHour;
    }

     static int getMaxC() {
        return maxC;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }


    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public Server(){
            customers = new LinkedBlockingQueue<Customer>();
            waitingPeriod = new AtomicInteger(0);
        }

    public void addCustomer(Customer newCustomer){
            customers.add(newCustomer);
            noCustomers++;
            waitingPeriod.addAndGet(newCustomer.getProcessingTime());
        }
    public void peakH(int n,Customer customer){
        if(n>maxC) {
            maxC = n;
            peakHour = customer.getArrivalTime();
        }
        aux++;
        avgProcessingTime=avgProcessingTime+ customer.getProcessingTime();
        avgWaitingTime=avgWaitingTime+customer.getWaitingTime();
    }
    public static double avgt(){
        avgProcessingTime = avgProcessingTime/aux;
        return avgProcessingTime;
    }
    public static double avgWaiting(){
        avgWaitingTime=avgWaitingTime/aux;
        return avgWaitingTime;
    }
    public void run() {
            while(true){
                Customer nextC;
                try {
                    nextC=customers.take();
                    peakH(noCustomers,nextC);
                    Thread.sleep(nextC.getProcessingTime()*1000);
                    Main.view.getTextArea1().append("\nTime(s): "+ nextC.getWaitingTime() +"\nCustomer "+nextC.getNo()+ " just left, he had processing time " + nextC.getProcessingTime()+" and waiting time "+nextC.getWaitingTime());
                    noCustomers--;
                    Main.view.getTextArea1().append("\n"+noCustomers);
                    waitingPeriod.getAndAdd(-nextC.getProcessingTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
    }
}
