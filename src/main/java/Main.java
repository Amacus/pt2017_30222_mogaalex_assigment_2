/**
 * Created by Amacus5 on 31.03.2017.
 */
import javax.swing.*;

public class Main {
    public static View view = new View();
    public static void main(String[] args) {
        JFrame frame = new JFrame("Simulation");
        frame.setContentPane( view.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
